n, k = map(int, input().split())

a = list(map(int, input().split()))
a.sort()
b = list(map(int, input().split()))

for value in b:
    mid = len(a) // 2
    left = 0
    right = len(a) - 1
    while a[mid] != value and left <= right:
        if value > a[mid]:
            left = mid + 1
        else:
            right = mid - 1
        mid = (left + right) // 2

    if left > right:
        print("NO")
    else:
        print("YES")