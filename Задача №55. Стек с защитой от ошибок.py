def push(n):
    stack.append(n)
    print('ok')

def pop():
    if len(stack) > 0:
        print(stack.pop())
    else:
        print('error')

def back():
    if len(stack) > 0:
        print(stack[len(stack) - 1])
    else:
        print('error')

def size():
    print(len(stack))

def clear():
    stack.clear()
    print('ok')

def exit():
    print('bye')

def check(arg):
    if arg[0] == 'push':
        push(int(arg[1]))
    if arg[0] == 'pop':
        pop()
    if arg[0] == 'back':
        back()
    if arg[0] == 'size':
        size()
    if arg[0] == 'clear':
        clear()

def check_for_exit(arg):
    if arg[0] == 'exit':
        return True

stack = []

while 1:
    a = list(input(). split())
    if check_for_exit(a):
        exit()
        break
    check(a)
