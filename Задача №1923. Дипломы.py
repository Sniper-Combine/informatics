w,h,n=str(input()).split()

w=int(w)
h=int(h)
n=int(n)
l=0
r=max(w,h)*n
while r-l>0:
    m=(l+r)//2
    if (m//h)*(m//w)>=n:
        r=m
    else:
        l=m+1
print(r)