def swap(arr, i, j):                                                                                                    # Это сам алгоритм сортировки, на него плевать,
    arr[i], arr[j] = arr[j], arr[i]                                                                                     # потому что в предыдущей задаче этот же алгоритм выполняется
                                                                                                                        # за 2 секунды
def siftDown(arr, i, upper):
    while 1:
        l, r = i*2+1, i*2+2
        if max(l, r) < upper:
            if arr[i] >= max(arr[l], arr[r]):
                break
            elif arr[l] > arr[r]:
                swap(arr, i, l)
                i = l
            else:
                swap(arr, i, r)
                i = r
        elif l < upper:
            if arr[l] > arr[i]:
                swap(arr, i, l)
                i = l
            else:
                break
        elif r < upper:
            if arr[r] > arr[i]:
                swap(arr, i, r)
                i = r
            else:
                break
        else:
            break

def heapsort(arr):
    for j in range((len(arr)-2)//2, -1, -1):
        siftDown(arr, j, len(arr))

    for j in range(len(arr)-1, 0, -1):
        swap(arr, 0, j)
        siftDown(arr, 0, j)

def Insert(k):
                                                                                                                        #Две функции по условию: обычное добавление в пустой список
    lst.append(k)

def Extract():                                                                                                          # и удаление из списка с возвратом значения(без индекса, потому что lst.pop() удаляет последний элемент списка,
                                                                                                                        # а после сортировки последний элемент - всегда максимальный
    return lst.pop()

lst = []
n = int(input())                                                                                                        #Тут вводим количество команд


for i in range(n):                                                                                                      #Ну и собственно цикл for, производящий количество итераций равное числу команд
    a = list(map(int, input().split()))                                                                                 #На каждой итерации, переменная а ссылается на новый список, который мы мапим из инпута
    if a[0] == 0:                                                                                                       #Если первый элемент списка равен 0, то мы добавляем второй элемент списка в сортируемый список
        Insert(a[1])
        heapsort(lst)                                                                                                   #Произвожу сортировку после каждого вызова функции Insert(), если производить сортировку 1 раз пекред вызовом функции Extract(), время выполнения проги не уменьшается
    if a[0] == 1:
        print(Extract())                                                                                                #Если первый элемент списка равен 1, вызываем функцию Extract() и выводим в консольке максимальное число.

#Я хз, зачем я тут все расписывал, ты и так всю эту изи хню поймешь. Я грешу на цикл, мб его можно на что-то еще менее требовательное заменить?



