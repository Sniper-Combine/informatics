n = int(input())
lst = list(map(int, input(). split()))

minv = min(lst)
maxv = max(lst)
totalv = maxv - minv + 1
count = [0] * totalv

for i in lst:
    count[i - minv] += 1

ind = 0
for i in range(len(count)):
    for j in range(count[i]):
        lst[ind] = i+minv
        ind += 1

print(*lst)
