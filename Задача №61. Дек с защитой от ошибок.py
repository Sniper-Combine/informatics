from collections import deque

deck = deque()

def check_len():
    if len(deck) == 0:
        print('error')
    else:
        return True

def push_front(val):
    deck.appendleft(val)
    print('ok')

def push_back(val):
    deck.append(val)
    print('ok')

def pop_front():
    if check_len():
        print(deck.popleft())

def pop_back():
    if check_len():
        print(deck.pop())

def front():
    if check_len():
        print(deck[0])

def back():
    if check_len():
        print(deck[len(deck) - 1])

def size():
    print(len(deck))

def clear():
    deck.clear()
    print('ok')

a = list(input().split())

while a[0] != 'exit':
    if a[0] == 'push_front':
        push_front(a[1])
    if a[0] == 'push_back':
        push_back(a[1])
    if a[0] == 'pop_front':
        pop_front()
    if a[0] == 'pop_back':
        pop_back()
    if a[0] == 'front':
        front()
    if a[0] == 'back':
        back()
    if a[0] == 'size':
        size()
    if a[0] == 'clear':
        clear()
    a = list(input().split())

print('bye')