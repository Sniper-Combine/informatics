class Solution:
    def isValid(self, s: str) -> bool:

        for i in s:
            if i in '([{':
                self.stack_append(i)
            if i in ')]}':
                if not self.stack_pop(i):
                    return False
        if len(self.lst) > 0:
            return False

        return True

    def __init__(self):
        self.lst = []

    def stack_append(self, sc):
        self.lst.append(sc)

    def stack_pop(self, sc):
        if len(self.lst) == 0:
            return False
        if sc == ')' and self.lst[-1] == '(':
            self.lst.pop()
            return True
        if sc == ']' and self.lst[-1] == '[':
            self.lst.pop()
            return True
        if sc == '}' and self.lst[-1] == '{':
            self.lst.pop()
            return True
        return False

c = Solution()

print('yes' if c.isValid(input()) else 'no')
