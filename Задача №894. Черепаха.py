import math

v, d = map(int, input().split())
n = int(input())
R = []
T = []
z = 0
for i in range(n):
    x, t =input().split()
    x = int(x)
    h, m = map(int, t.split(":"))
    R.append(x-z)
    T.append(float(h*60+m))
    z = x

L = 0
PR = max(T)
for i in range(100):
    M = (L+PR)/2
    t = M
    for i in range(len(R)-1):
        t += R[i]/v
        if t >= T[i]:
            t += d
    t += R[-1]/v
    if t == T[-1]:
        break
    elif t > T[-1]:
        PR = M
    else:
        L = M

t = 2*z/v+n*d+PR
t = math.ceil(t)
h = t//60
m = t%60
print(str(h).zfill(2)+":"+str(m).zfill(2))