def push(n):
    stack.insert(0, n)
    print('ok')

def pop():
    print(stack.pop())

def front():
    print(stack[len(stack) - 1])

def size():
    print(len(stack))

def clear():
    stack.clear()
    print('ok')

def exit():
    print('bye')

def check(arg):
    if arg[0] == 'push':
        push(int(arg[1]))
    if arg[0] == 'pop':
        pop()
    if arg[0] == 'front':
        front()
    if arg[0] == 'size':
        size()
    if arg[0] == 'clear':
        clear()
    if arg[0] == 'exit':
        exit()

def check_for_exit(arg):
    if arg[0] == 'exit':
        return True

stack = []

a = list(input().split())

while a[0] != 'exit':
    check(a)
    a = list(input().split())

exit()


