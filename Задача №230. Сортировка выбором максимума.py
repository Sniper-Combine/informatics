a = int(input())
lst = list(map(int, input().split()))
# [8, 4, 12, 6, 3, 2, 10, 7] [8 4 12 6 3 2 10 7] -> [2 3 4 6 7 8 10 12]

for i in range(len(lst) - 1):
    temp = lst[i]
    indx = i
    for j in range(i, len(lst)):
        if lst[j] < temp:
            temp = lst[j]
            indx = j
    first_val = lst[i]
    sec_val = lst[indx]
    lst[i], lst[indx] = sec_val, first_val

print(*lst)




