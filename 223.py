N = int(input())
lst = list(map(int, input().split()))
x = int(input())

counter = 0

for i in range(N):
    if lst[i] == x:
        counter += 1

print(counter)