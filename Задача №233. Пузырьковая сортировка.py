a = int(input())
lst = list(map(int,input().split()))

for i in range(len(lst) - 1):
    temp = 0
    for j in range(len(lst) - 1):
        if lst[j] > lst[j + 1]:
            temp = lst[j]
            lst[j] = lst[j + 1]
            lst[j + 1] = temp

print(*lst)


